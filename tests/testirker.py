# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8 ai ts=4 sts=4 et sw=4
# Copyright 2012 Neil Muller
# GPL 2+ - see COPYING for details
from unittest import TestLoader
from SocketServer import TCPServer, StreamRequestHandler
import threading

from bzrlib import config
from bzrlib.bzrdir import BzrDir
from bzrlib.tests import TestCaseInTempDir
from bzrlib.plugins.bzrirker.irkerhook import IrkerSender


def test_suite():
    return TestLoader().loadTestsFromName(__name__)

sample_config = ("[DEFAULT]\n"
        "irker_project=bzrirker\n"
        "irker_server=localhost\n"
        "irker_port=6677\n"
        "irker_channels=irc://chat.example.test/commits\n")

no_project_config = ("[DEFAULT]\n"
        "irker_channels=irc://chat.example.test/commits\n")

colour_config = ("[DEFAULT]\n"
        "irker_project=bzrirker\n"
        "irker_colours=mIRC\n"
        "irker_channels=irc://chat.example.test/commits\n")


class MyHandler(StreamRequestHandler):

    def handle(self):
        data = self.rfile.read()
        self.server.data = data


class TestConfigured(TestCaseInTempDir):

    def test_no_colours(self):
        irker = self.get_irker()
        self.assertEqual({
            'bold': '', 'green': '', 'blue': '', 'red': '',
            'yellow': '', 'brown': '', 'magenta': '', 'cyan': '',
            'reset': ''}, irker.colours())

    def test_colours(self):
        irker = self.get_irker(colour_config)
        self.assertEqual({
            'bold': '\x02', 'green': '\x0303', 'blue': '\x0302',
            'red': '\x0305', 'yellow': '\x0307', 'brown': '\x0305',
            'magenta': '\x0306', 'cyan': '\x0310', 'reset': '\x0F'},
            irker.colours())

    def test_message(self):
        irker = self.get_irker()
        self.assertEqual(
                'bzrirker: Sample <john@example.test> work * 1 /  : foo bar ',
                irker._format())

    def test_no_project(self):
        irker = self.get_irker(no_project_config)
        self.assertEqual(
                'No Project name set: Sample <john@example.test>'
                ' work * 1 /  : foo bar ',
                irker._format())

    def test_message_colour(self):
        irker = self.get_irker(colour_config)
        self.assertEqual(
                '\x02bzrirker:\x0f \x0303Sample <john@example.test>\x0f'
                ' work * \x021\x0f /  \x02\x0f: foo bar ', irker._format())

    def test_send(self):
        irker = self.get_irker()
        server = self.get_server()
        irker.send()
        while server.data is None:
            pass
        server.shutdown()
        self.assertEqual(server.data,
                '{"to": ["irc://chat.example.test/commits"]'
                ', "privmsg": "bzrirker: Sample <john@example.test>'
                ' work * 1 /  : foo bar "}\n')

    def get_irker(self, text=sample_config):
        my_config = config.MemoryStack(text)
        self.branch = BzrDir.create_branch_convenience('.')
        tree = self.branch.bzrdir.open_workingtree()
        tree.commit('foo bar\nfuzzy\nwuzzy', rev_id='A',
                allow_pointless=True,
                timestamp=1,
                timezone=0,
                committer="Sample <john@example.test>",
                )
        irker = IrkerSender(self.branch, 'A', my_config)
        # We're not calling send, so do the setup stuff here
        irker._setup_revision_and_revno()
        return irker

    def get_server(self):
        server = TCPServer(('localhost', 6677), MyHandler)
        server.data = None
        thread = threading.Thread(target=server.serve_forever)
        thread.start()
        return server
