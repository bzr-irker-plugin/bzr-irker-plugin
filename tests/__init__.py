# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8 ai ts=4 sts=4 et sw=4
# Copyright 2012 Neil Muller
# GPL 2+ - see COPYING for details

from unittest import TestLoader, TestSuite


def test_suite():
    result = TestSuite()
    import testirker

    loader = TestLoader()
    result.addTests(loader.loadTestsFromModule(testirker))
    return result
